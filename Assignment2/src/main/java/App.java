import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;

public class App extends Canvas implements Runnable, KeyListener{
    private boolean isRunnig = false;
    private Thread thread;
    private Controler controller;
    public App(){
        Dimension dim = new Dimension(1100, 500);
        setPreferredSize(dim);
        setMinimumSize(dim);
        setMaximumSize(dim);
        controller = new Controler(5,1,1.2, 5);
        addKeyListener(this);
    }
    public synchronized void start(){
        if(isRunnig)
            return;
        isRunnig = true;
        thread = new Thread(this);
        thread.start();
        controller.start();
    }
    public synchronized void stop(){
        if(!isRunnig)
            return;
        isRunnig = true;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private void render(){
        BufferStrategy bs = getBufferStrategy();
        if(bs == null){
            createBufferStrategy(3);
            return;
        }
        Graphics g = bs.getDrawGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0,0, 1100, 500);
        controller.render(g);
        g.dispose();
        bs.show();
    }
    public void run() {
        requestFocus();
        int fps = 0;
        double timer = System.currentTimeMillis();
        long lastTime = System.nanoTime();
        double targetRefresh = 60.0;
        double delta = 0;
        double ns = 1e9 / targetRefresh;
        while(isRunnig){
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while(delta >= 1){
                render();
                fps++;
                delta--;
            }
            if(System.currentTimeMillis() - timer >= 1000){
                System.out.println(fps);
                fps = 0;
                timer += 1000;
            }
        }
        stop();
    }

    public static void main(String[] args){
        App application = new App();
        JFrame frame = new JFrame("Queue simulator");
        frame.setSize(1100, 600);
        JPanel apppanel = new JPanel(new FlowLayout());
        apppanel.add(application);
        JPanel bottom = new JPanel(new FlowLayout());
        bottom.add(application.controller.panel1);
        bottom.add(application.controller.panel2);
        JPanel main = new JPanel();
        main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
        main.add(apppanel);
        main.add(bottom);
        frame.setContentPane(main);
        frame.setResizable(false);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        application.start();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("Key was pressed");
        if(e.getKeyCode() == KeyEvent.VK_1){
            if(controller.queues.get(0).getIsRunning())
                controller.queues.get(0).setIsRunning(false);
            else
                controller.queues.get(0).setIsRunning(true);
        }
        if(e.getKeyCode() == KeyEvent.VK_2){
            if(controller.queues.get(1).getIsRunning())
                controller.queues.get(1).setIsRunning(false);
            else
                controller.queues.get(1).setIsRunning(true);
        }
        if(e.getKeyCode() == KeyEvent.VK_3){
            if(controller.queues.get(2).getIsRunning())
                controller.queues.get(2).setIsRunning(false);
            else
                controller.queues.get(2).setIsRunning(true);
        }
        if(e.getKeyCode() == KeyEvent.VK_4){
            if(controller.queues.get(3).getIsRunning())
                controller.queues.get(3).setIsRunning(false);
            else
                controller.queues.get(3).setIsRunning(true);
        }
        if(e.getKeyCode() == KeyEvent.VK_5){
            if(controller.queues.get(4).getIsRunning())
                controller.queues.get(4).setIsRunning(false);
            else
                controller.queues.get(4).setIsRunning(true);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
