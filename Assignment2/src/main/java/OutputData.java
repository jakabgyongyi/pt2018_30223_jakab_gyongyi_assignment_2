import javax.swing.*;
import java.awt.*;
import java.sql.Time;

public class OutputData extends JPanel {
    private JLabel totalCustomers;
    private JLabel averageServiceTime;
    private JLabel averageWaitingTime;
    private JLabel totalEmptyTime;
    private JLabel time;
    public OutputData(){
        setLayout(new GridLayout(6,2,2,2));
        JLabel label1 = new JLabel("Total number of Customers: ");
        JLabel label2 = new JLabel ("Average service time: ");
        JLabel label3 = new JLabel ("Average waiting time: ");
        JLabel label4 = new JLabel("Total empty time: ");
        JLabel label5 = new JLabel ("Curent moment: ");
        JLabel label = new JLabel("Application start: "+new Time(System.currentTimeMillis()));
        totalCustomers = new JLabel();
        averageServiceTime = new JLabel();
        averageWaitingTime = new JLabel();
        totalEmptyTime = new JLabel();
        time = new JLabel();
        add(label1);
        add(totalCustomers);
        add(label2);
        add(averageServiceTime);
        add(label3);
        add(averageWaitingTime);
        add(label4);
        add(totalEmptyTime);
        add(label5);
        add(time);
        add(label);
    }
    public void setTotalCustomers(int totalCustomers){
        this.totalCustomers.setText(Integer.toString(totalCustomers));
    }
    public void setAverageServiceTime(float averageServiceTime){
        this.averageServiceTime.setText(Float.toString(averageServiceTime)+" s");
    }
    public void setAverageWaitingTime(float averageWaitingTime){
        this.averageWaitingTime.setText(Float.toString(averageWaitingTime)+" s");
    }
    public void setTotalEmptyTime(double totalEmptyTime){
        this.totalEmptyTime.setText(Double.toString(totalEmptyTime)+" s");
    }
    public void setTime(){
        this.time.setText(new Time(System.currentTimeMillis()).toString());
    }
}
