import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class InputData extends JPanel implements ActionListener, ChangeListener{
    private JLabel feederSpeedIn;
    private JSlider speedfixer;
    private JLabel minServiceTimeIn;
    private JButton minPlus, minMinus;
    private JLabel maxServiceTimeIn;
    private JButton maxPlus, maxMinus;
    private JLabel nrOfqueueIn;
    private JCheckBox mode;
    private JLabel modelabel;
    public InputData(){
        setLayout(new GridLayout(4,4, 2,2));
        JLabel feederSpeed = new JLabel("Feeder speed");
        JLabel minServiceTime = new JLabel("Min Service time");
        JLabel maxServiceTime = new JLabel("Max Service time");
        JLabel nrOfqueue = new JLabel("Nr. active queues");
        mode = new JCheckBox("Select Mode");
        mode.addActionListener(this);
        mode.setSelected(true);
        modelabel = new JLabel("Service Time");
        feederSpeedIn = new JLabel();
        speedfixer = new JSlider();
        speedfixer.addChangeListener(this);
        speedfixer.setMaximum(30);
        speedfixer.setMinimum(5);
        speedfixer.setValue(5);
        speedfixer.setMajorTickSpacing(5);
        speedfixer.setPaintTicks(true);
        minServiceTimeIn = new JLabel();
        minMinus = new JButton("-");minMinus.addActionListener(this);
        minPlus = new JButton("+");minPlus.addActionListener(this);
        maxServiceTimeIn = new JLabel();
        maxMinus = new JButton("-");maxMinus.addActionListener(this);
        maxPlus = new JButton("+");maxPlus.addActionListener(this);
        nrOfqueueIn = new JLabel();
        add(feederSpeed);
        add(feederSpeedIn);
        add(speedfixer);
        add(new Label());

        add(minServiceTime);
        add(minServiceTimeIn);
        add(minMinus);
        add(minPlus);

        add(maxServiceTime);
        add(maxServiceTimeIn);
        add(maxMinus);
        add(maxPlus);

        add(nrOfqueue);
        add(nrOfqueueIn);
        add(mode);
        add(modelabel);
    }
    public void setFeederSpeedIn(double speed) {
        this.feederSpeedIn.setText(Double.toString(speed));
    }
    public void setMinServiceTimeIn(int minTime) {
        this.minServiceTimeIn.setText(Integer.toString(minTime));
    }
    public void setMaxServiceTimeIn(int maxTime) {
        this.maxServiceTimeIn.setText(Integer.toString(maxTime));
    }
    public void setNrOfqueueIn(int nrQueues) {
        this.nrOfqueueIn.setText(Integer.toString(nrQueues));
    }

    public double getFeederSpeedIn() {
        if(this.feederSpeedIn.getText().isEmpty())
            return 0;
        try{
            return Double.parseDouble(this.feederSpeedIn.getText());
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
        return 0;
    }

    public int getMinServiceTimeIn() {
        if(minServiceTimeIn.getText().isEmpty())
            return 0;
        try{
            return Integer.parseInt(this.minServiceTimeIn.getText());
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
        return 0;
    }

    public int getMaxServiceTimeIn() {
        if(maxServiceTimeIn.getText().isEmpty())
            return 0;
        try{
            return Integer.parseInt(this.maxServiceTimeIn.getText());
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == minMinus)
            if(getMinServiceTimeIn() > 1)
                setMinServiceTimeIn(getMinServiceTimeIn() - 1);
        if(e.getSource() == minPlus)
            setMinServiceTimeIn(getMinServiceTimeIn() + 1);
        if(e.getSource() == maxMinus)
            if(getMaxServiceTimeIn() > 1)
                setMaxServiceTimeIn(getMaxServiceTimeIn() - 1);
        if(e.getSource() == maxPlus)
            setMaxServiceTimeIn(getMaxServiceTimeIn() + 1);
        if(e.getSource() == mode){
            if(mode.isSelected())
                modelabel.setText("Service Time");
            else
                modelabel.setText("Customer number");
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if(e.getSource() == speedfixer){
            if(!speedfixer.getValueIsAdjusting())
                setFeederSpeedIn(speedfixer.getValue()/10.0);
        }
    }
    public int getMode(){
        if(mode.isSelected())
            return 1;
        else
            return 0;
    }
}
