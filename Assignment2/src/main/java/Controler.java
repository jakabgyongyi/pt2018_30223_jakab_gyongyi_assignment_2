import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Logger;

public class Controler implements Runnable {
    private boolean isRunning = false;
    private int serviceTimeMax;
    private int serviceTimeMin;
    private double speed;
    private int mode;
    ArrayList<Queue> queues;
    private Thread thread;
    InputData panel1;
    OutputData panel2;
    private Logger logofevents = Logger.getLogger(EventLogger.class.getName());

    public Controler(int sTMax, int sTMin, double speed, int nrofqueues) {
        serviceTimeMin = sTMin;
        serviceTimeMax = sTMax + 1;
        this.speed = speed;
        queues = new ArrayList<Queue>();
        for(int i = 1; i <= nrofqueues; i++){
            if(i <= 3)
                queues.add(new Queue("Line "+i, 20, true));
            else
                queues.add(new Queue("Line "+i, 20, false));
        }
        panel1 = new InputData();
        panel2 = new OutputData();
        panel1.setFeederSpeedIn(speed);
        panel1.setMinServiceTimeIn(sTMin);
        panel1.setMaxServiceTimeIn(sTMax);
        panel1.setNrOfqueueIn(3);
        this.mode = 1;
    }

    public void run() {
        Random rand = new Random();
        int serviceTime;
        logofevents.info("CONTROLLER WAS ACTIVATED");
        while (isRunning) {
            speed = panel1.getFeederSpeedIn();
            serviceTimeMin = panel1.getMinServiceTimeIn();
            serviceTimeMax = panel1.getMaxServiceTimeIn();
            do {
                serviceTime = rand.nextInt(serviceTimeMax);
            } while (serviceTime < serviceTimeMin);
            Customer person = new Customer(serviceTime, totalNrOfCustomers() + 1);
            System.out.println(person);
            Queue insert = analizeQueues();
            if (insert.getNumberOfCustomers() < insert.getCAPACITY())
                insert.addCustomer(person);
            try {
                Thread.sleep((int) (1000 / speed));
            } catch (InterruptedException e) {
                logofevents.severe("CONTROLLER WAS INTERRUPTED");
            }
        }
    }

    private Queue analizeQueues() {
        mode = panel1.getMode();
        Queue line = queues.get(0);
        for (Queue itr : queues) {
            if(mode == 1) {
                if (itr.calculateTotalServiceTime() < line.calculateTotalServiceTime() && itr.getIsRunning())
                    line = itr;
            }
            else{
                if(itr.getSize() < line.getSize() && itr.getIsRunning())
                    line = itr;
            }
        }
        return line;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
        for (Queue itr : queues) {
            itr.start();
        }
        this.isRunning = true;
    }

    public void render(Graphics g) {
        BufferedImage img = null;
        String path = "C:\\Users\\jakabgyongyi\\IdeaProjects\\Assignment2\\src\\main\\java\\images\\entry.png";
        try {
            img = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        g.drawImage(img, 20, 50, null);
        int x =30, y=40;
        for (Queue itr : queues) {
            itr.render(g,x,y);
            x += 100;
            y += 100;
        }
        panel2.setTotalCustomers(totalNrOfCustomers());
        panel2.setTime();
        panel2.setAverageServiceTime(averageServiceTime());
        panel2.setAverageWaitingTime(averageWaitingTime());
        panel2.setTotalEmptyTime(totalEmptyTime());
    }

    private int totalNrOfCustomers() {
        int count = 0;
        for (Queue itr : queues) {
            count += itr.getTotalCustomers();
        }
        return count;
    }

    private float averageWaitingTime() {
        float count = 0;
        for (Queue itr : queues) {
            count += itr.getAverageWaitingTime();
        }
        return count / 3;
    }

    private float averageServiceTime() {
        float count = 0;
        for (Queue itr : queues) {
            count += itr.getAverageServiceTime();
        }
        return count / 3;
    }

    private double totalEmptyTime() {
        double count = 0;
        for (Queue itr : queues) {
            count += itr.getTotalEmptyTime();
        }
        return count;
    }
}
