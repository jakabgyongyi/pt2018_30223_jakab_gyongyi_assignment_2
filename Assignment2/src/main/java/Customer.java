import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Time;

public class Customer{
    private long arrivalTime;
    private int serviceTime;
    private int cardinal;

    public Customer(int serviceTime,int cardinal) {
        this.serviceTime = serviceTime;
        this.arrivalTime = System.currentTimeMillis();
        this.cardinal = cardinal;
    }
    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append("Time of arrival: ").append(new Time(arrivalTime)).append("; ");
        str.append("Service time: ").append(serviceTime).append(" min").append("; ");
        return str.toString();
    }
    public void render(Graphics g,int x,int y){
        BufferedImage img = null;
        String path = "C:\\Users\\jakabgyongyi\\IdeaProjects\\Assignment2\\src\\main\\java\\images\\customer.png";
        try {
            img = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        g.drawImage(img, x, y, null);
    }
    public int getServicetime() {
        return serviceTime;
    }
    public long getArrivaltime(){
        return arrivalTime;
    }
    public int getCardinal(){
        return cardinal;
    }
}
