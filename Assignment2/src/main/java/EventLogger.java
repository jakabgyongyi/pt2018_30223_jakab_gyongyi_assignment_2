import java.io.IOException;
import java.util.logging.*;

public class EventLogger {
    private Logger eventlogger;
    private FileHandler fileHandler = null;
    public void initialize(){
        try {

            fileHandler = new FileHandler("EventLogger.log", true);
        } catch (IOException e) {
            System.out.println("Cannot initialize a filehandler for further interaction");
        }
        eventlogger = Logger.getLogger("");
        fileHandler.setFormatter(new SimpleFormatter());
        eventlogger.addHandler(fileHandler);
        eventlogger.setLevel(Level.CONFIG);
    }
}
