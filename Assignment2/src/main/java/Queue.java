import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

public class Queue implements Runnable {
    private boolean isRunning = false;
    private Thread thread;
    private String name;
    private int CAPACITY;
    private BlockingQueue<Customer> queue;
    private int totalCustomers;
    private int totalServiceTime;
    private long waitingTime;
    private double totalEmptyTime;
    private Logger logofevents = Logger.getLogger(EventLogger.class.getName());
    public Queue(String name, int CAPACITY, boolean isRunning){
        this.isRunning = isRunning;
        this.CAPACITY = CAPACITY;
        queue = new ArrayBlockingQueue<>(CAPACITY);
        this.name = name;
        totalCustomers = 0;
        totalServiceTime = 0;
        waitingTime = 0;
    }
    public void run() {
        if(isRunning)logofevents.info(Thread.currentThread().getName()+" WAS ACTIVATED");
        while(isRunning){
            if(!queue.isEmpty()){
                Customer current = queue.peek();
                logofevents.info("Handling service " + current.getCardinal()+" of time: " + current.getServicetime() + " min.");
                try {
                    Thread.sleep(1000*current.getServicetime());
                } catch (InterruptedException e) {
                    logofevents.severe("EXECUTION OF SERVICE " + current.getCardinal() + " WAS INTERRUPTED!");
                }
                waitingTime += (System.currentTimeMillis() - current.getArrivaltime())/1000;
                queue.poll();
                logofevents.info("Service " + current.getCardinal() + " was successfully handled");
            }
            else{
                long startempty = System.currentTimeMillis();
                long stopempty  = startempty;
                while(queue.isEmpty())
                    stopempty = System.currentTimeMillis();
                if((stopempty - startempty)/1000 != 0)
                    logofevents.warning(name + " was empty for " + (stopempty - startempty)/1000 + "sec");
                totalEmptyTime += (stopempty - startempty)/1000;
            }
        }
    }
    public void render(Graphics g, int x,int y){
        BufferedImage img = null;
        String path = null;
        if(isRunning)
            path = "C:\\Users\\jakabgyongyi\\IdeaProjects\\Assignment2\\src\\main\\java\\images\\cashon.png";
        else
            path = "C:\\Users\\jakabgyongyi\\IdeaProjects\\Assignment2\\src\\main\\java\\images\\cashoff.png";
        try {
            img = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        g.drawImage(img, 1000, x, null);
        int i=950;
        for(Customer itr: queue){
            itr.render(g,i, y);
            i -= 50;
        }
    }
    public void addCustomer(Customer obj){
        queue.add(obj);
        totalCustomers++;
        totalServiceTime += obj.getServicetime();
        logofevents.info(obj + "Located in: " + name);
    }
    public int calculateTotalServiceTime(){
        int time = 0;
        for(Customer itr: queue){
            time += itr.getServicetime();
        }
        return time;
    }
    public int getSize(){
        return queue.size();
    }
    public void start(){
        if(thread == null){
            thread = new Thread(this, name);
            thread.start();
        }
    }
    public int getNumberOfCustomers(){
        return queue.size();
    }
    public int getCAPACITY(){
        return CAPACITY;
    }
    public int getTotalCustomers(){
        return totalCustomers;
    }
    public float getAverageServiceTime(){
        if(totalCustomers == 0)return 0;
        return totalServiceTime/totalCustomers;
    }
    public long getAverageWaitingTime(){
        if(totalCustomers == queue.size())return waitingTime;
        return waitingTime/(totalCustomers - queue.size());
    }
    public double getTotalEmptyTime(){
        return totalEmptyTime;
    }
    public boolean getIsRunning(){
        return this.isRunning;
    }
    public void setIsRunning(boolean isRunning){
        this.isRunning = isRunning;
        logofevents.info("IsRunning was set to :"+isRunning);
    }
}
